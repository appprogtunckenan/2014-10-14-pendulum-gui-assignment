import numpy as np
import scipy.integrate as integrate
import matplotlib.animation as animation
from sympy import symbols
from sympy.physics.mechanics import *
from sympy import Dummy, lambdify
from numpy.linalg import solve
from scipy.integrate import odeint
from numpy import ones,zeros, cos, sin, arange, around, pi, array, hstack, zeros, linspace
from matplotlib import pyplot as plt
from matplotlib import animation

class Pendulum_Simulation:
    def __init__(self):

        self.N = 2 # number of links
        self.G =  9.81 # acceleration due to gravity, in m/s^2
        self.T0 = 0 # seconds
        self.Tf = 20 # seconds
        self.dt = 0.05 # seconds

        #Equally distribute masses and link lengths
        self.L = (1.0/self.N)*ones((self.N))
        self.M = ones((self.N))

    def simulate(self): 

        q = dynamicsymbols('q:' + str(self.N))  # Generalized coordinates
        u = dynamicsymbols('u:' + str(self.N))  # Generalized speeds
            
        m = symbols('m:' + str(self.N))         # Mass of each bob
        l = symbols('l:' + str(self.N))             # Length of each link
        g, t = symbols('g t')                  # Gravity and time

        I = ReferenceFrame('I')                # Inertial reference frame
        O = Point('O')                         # Origin point
        O.set_vel(I, 0)                        # Origin's velocity is zero


        frames = []                              # List to hold the n + 1 frames
        points = []
        particles = []                         # List to hold the n + 1 particles
        forces = []
        kindiffs = []          # List to hold kinematic ODE's


        for i in range(self.N):
            Bi = I.orientnew('B' + str(i), 'Axis', [q[i], I.z])   # Create a new frame
            Bi.set_ang_vel(I, u[i] * I.z)                         # Set angular velocity
            frames.append(Bi)                                         # Add it to the frames list

            if i ==0:
                Pi = O.locatenew('P' + str(i), l[i] * Bi.x)  # Create a new point
                Pi.v2pt_theory(O, I, Bi)                         # Set the velocity
                points.append(Pi)                                         # Add it to the points list
            else:
                Pi = points[-1].locatenew('P' + str(i), l[i] * Bi.x)  # Create a new point
                Pi.v2pt_theory(points[-1], I, Bi)                         # Set the velocity
                points.append(Pi)                                         

            Pai = Particle('Pa' + str(i), Pi, m[i])           # Create a new particle
            particles.append(Pai)                                     # Add it to the particles list

            forces.append((Pi, -m[i] * g * I.y))                  # Set the force applied at the point
                
            kindiffs.append(q[i].diff(t) - u[i])              # Define the kinematic ODE:  dq_i / dt - u_i = 0

        kane = KanesMethod(I, q_ind=q, u_ind=u, kd_eqs=kindiffs) # Initialize the object
        fr, frstar = kane.kanes_equations(forces, particles)     # Generate EoM's fr + frstar = 0

        #Symbolic equations commented out.
        # print fr
        # print frstar

        G=self.G
        parameters = [g]                       # Parameter definitions starting with gravity and the first bob
        parameter_vals = [G]            # Numerical values for the first two


        self.L = (1.0/self.N)*ones((self.N))
        self.M = ones((self.N))
        L_sum = np.sum(self.L)

        for i in range(self.N):                       
                parameters += [l[i],m[i]]                       # Then each mass and length        
                parameter_vals += [(self.L[i])/L_sum,self.M[i]]


        dynamic = q + u                                                # Make a list of the states
        dummy_symbols = [Dummy() for i in dynamic]                     # Create a dummy symbol for each variable
        dummy_dict = dict(zip(dynamic, dummy_symbols))                 
        kindiff_dict = kane.kindiffdict()                              # Get the solved kinematical differential equations
        M = kane.mass_matrix_full.subs(kindiff_dict).subs(dummy_dict)  # Substitute into the mass matrix 
        F = kane.forcing_full.subs(kindiff_dict).subs(dummy_dict)      # Substitute into the forcing vector
        M_func = lambdify(dummy_symbols + parameters, M)               # Create a callable function to evaluate the mass matrix 
        F_func = lambdify(dummy_symbols + parameters, F)               # Create a callable function to evaluate the forcing vector 

        #time values
        t_0 = self.T0
        t_f = self.Tf
        dt  = self.dt

        #initial states
        self.q_init = pi/4*ones(len(q))
        self.u_init = 1e-3*ones(len(u))

        def RHS_eq(x, t, args):
            arguments = hstack((x, args))     # States, input, and parameters
            dx = array(solve(M_func(*arguments), # Solving for the derivatives
            F_func(*arguments))).T[0]
                
            return dx
            

        coeff_t = 10/dt

        self.x0 = hstack(( self.q_init, self.u_init  ))
        self.t = linspace(coeff_t * t_0, coeff_t*dt, coeff_t*t_f)                                          # Time vector
        self.y = odeint(RHS_eq, self.x0, self.t, args=(parameter_vals,))         # Actual integration

        # self.data = np.concatenate((self.y,self.x1,self.x2,self.y1,self.y2),axis=1)
        # self.data_indexes={"theta 1":0,"theta dot 1":2,"theta 2":1,"theta dot 2":3,"time":4,"point 1 x":5,"point 2 x":6,"point 1 y":7, "point 2 y":8}

    def get_data(self):
        return self.data
    def get_data_indexes(self):
        return self.data_indexes

    def init(self):

        # display the current time
        self.time_text = self.ax.text(0.04, 0.9, '', transform=self.ax.transAxes)

        # blank line for the pendulum
        self.line, = self.ax.plot([], [], lw=2, marker='o', markersize=6)

        self.time_text.set_text('')
        self.line.set_data([], [])
        return self.time_text, self.line

    def animate(self,i):

        # the number of pendulum bobs
        numpoints = self.y.shape[1] / 2

        # animation function: update the objects
        self.time_text.set_text('time = {:2.2f}'.format(self.t[i]))
        x = zeros((numpoints+1))
        y = zeros((numpoints+1))


        for j in arange(1, numpoints+1):

                x[j] = x[j-1] + self.L[j-1] * cos(self.y[i, j-1])
                y[j] = y[j-1] + self.L[j-1] * sin(self.y[i, j-1])
        self.line.set_data(x,y)


        return self.time_text, self.line

    def begin_animation(self):

        self.anim = animation.FuncAnimation(self.fig, self.animate, frames=len(self.t), init_func=self.init,
        interval=self.t[-1] / len(self.t) * 10, blit=True, repeat=False)

        # self.ani = animation.FuncAnimation(self.fig, self.animate, np.arange(1, len(self.y)),
        # interval=50, blit=True, init_func=self.init_animation)
        
    def generate_figure(self):
        self.fig = plt.figure()
        self.ax = plt.axes(xlim=(-1.1, 1.1), ylim=(-1.1, 1.1), aspect='equal')
        # self.ax = self.fig.add_subplot(111, autoscale_on=False, xlim=(-2, 2), ylim=(-2, 2))
        self.ax.grid()
        return self.fig

#ani.save('double_pendulum.mp4', fps=15, clear_temp=True)
def main():
    sim = Pendulum_Simulation()
    sim.generate_figure()
    sim.init()
    sim.simulate()
    #set gravity
    sim.G=9.81
    # set number of links
    sim.N=5
    sim.simulate()
    sim.begin_animation()
    plt.show()

if __name__ == "__main__":
    main()
